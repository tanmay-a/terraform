provider "aws" {
  profile = var.profile
  region  = var.master
  alias   = "master"
}
provider "aws" {
  profile = var.profile
  region  = var.worker
  alias   = "worker"
}