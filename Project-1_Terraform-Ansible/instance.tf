# Getting Linux AMI id using AWS SSM parameters

data "aws_ssm_parameter" "linux_ami" {
  provider = aws.master
  name     = "/aws/service/ami-amazon-linux-latest/amzn2-ami-hvm-x86_64-gp2"
}

data "aws_ssm_parameter" "linux_ami_oregon" {
  provider = aws.worker
  name     = "/aws/service/ami-amazon-linux-latest/amzn2-ami-hvm-x86_64-gp2"
}

#Creating key pair resource for master EC2 region which will be baked during EC2 creation

resource "aws_key_pair" "master-key" {
  provider   = aws.master
  key_name   = "mykey"
  public_key = file("~/.ssh/id_rsa.pub")
}

#Creating key pair resource for worker EC2 region which will be baked during EC2 creation

resource "aws_key_pair" "worker-key" {
  provider   = aws.worker
  key_name   = "mykey"
  public_key = file("~/.ssh/id_rsa.pub")
}

# Create and bootstrap EC2 instance for Jenkins-master in us-east-1

resource "aws_instance" "jenkins-master" {
  provider                    = aws.master
  ami                         = data.aws_ssm_parameter.linux_ami.value
  instance_type               = var.instance-type
  key_name                    = aws_key_pair.master-key.key_name
  associate_public_ip_address = true
  vpc_security_group_ids      = [aws_security_group.jenkins-sg.id]
  subnet_id                   = aws_subnet.subnet_1.id

  tags = {
    "Name" = "jenkins-master"
  }
  depends_on = [aws_main_route_table_association.set_master_route]
  provisioner "local-exec" {
    command = <<EOF
aws --profile ${var.profile} ec2 wait instance-status-ok --region ${var.master} --instance-ids ${self.id}
ansible-playbook --extra-vars 'passed_in_hosts=tag_Name_${self.tags.Name}' ansible_templates/jenkins-master-sample.yml
EOF
  }
}

# Create and bootstrap EC2 instance for Jenkins-Worker node in us-west-2

resource "aws_instance" "jenkins-worker" {
  provider                    = aws.worker
  ami                         = data.aws_ssm_parameter.linux_ami_oregon.value
  count                       = var.workers-count
  instance_type               = var.instance-type
  key_name                    = aws_key_pair.worker-key.key_name
  associate_public_ip_address = true
  vpc_security_group_ids      = [aws_security_group.jenkins-sg-oregon.id]
  subnet_id                   = aws_subnet.subnet_1_oregon.id

  tags = {
    "Name" = join("_", ["jenkins_worker", count.index + 1])
  }
  depends_on = [aws_main_route_table_association.set_master_route, aws_instance.jenkins-master]
  provisioner "local-exec" {
    command = <<EOF
aws --profile ${var.profile} ec2 wait instance-status-ok --region ${var.worker} --instance-ids ${self.id}
ansible-playbook --extra-vars 'passed_in_hosts=tag_Name_${self.tags.Name}' ansible_templates/jenkins-worker-sample.yml
EOF
  }
}