#Create SG for loadbalancers

resource "aws_security_group" "lb-sg" {
  provider    = aws.master
  name        = "lb-sg"
  description = "Allow http and https traffic"
  vpc_id      = aws_vpc.vpc_master.id

  ingress {
    description = "TLS from VPC"
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "HTTP from VPC"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "allow_connection from outside"
  }
}

#Create SG for Jenkins Master allowing TCP/8080 from * and TCP/22 from your IP

resource "aws_security_group" "jenkins-sg" {
  provider    = aws.master
  name        = "jenkins-sg"
  description = "Allow Jenkins 8080 and SSH"
  vpc_id      = aws_vpc.vpc_master.id

  ingress {
    description = "Allow 22 from our public IP"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = [var.external_ip]
  }

  ingress {
    description     = "Allow webserver-port from lb"
    from_port       = var.webserver-port
    to_port         = var.webserver-port
    protocol        = "tcp"
    security_groups = [aws_security_group.lb-sg.id]
  }

  ingress {
    description = "Allow all trafic from Worker subnet"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["192.168.1.0/24"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "allow_connection from outside"
  }
}

#SG for Jenkins worker allowing TCP/8080 from Mastwe and TCP/22 from external IP

resource "aws_security_group" "jenkins-sg-oregon" {
  provider    = aws.worker
  name        = "jenkins-sg-oregon"
  description = "Allow Jenkins 8080 and SSH"
  vpc_id      = aws_vpc.vpc_master_oregon.id

  ingress {
    description = "Allow 22 from our public IP"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = [var.external_ip]
  }

  ingress {
    description = "Allow all trafic from Worker subnet"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["10.0.1.0/24"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "allow_connection from outside"
  }
}

