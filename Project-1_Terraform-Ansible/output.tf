#Printing public IP of the Master Instance created in instance.tf

output "Jenkins_Master_IP" {
  value = aws_instance.jenkins-master.public_ip
}

#Printing public IP of the Worker Instance created in instance.tf

output "Jenkins_Worker_IP" {
  value = {
    for instance in aws_instance.jenkins-worker :
    instance.id => instance.public_ip
  }
}

#Add LB DNS name to outputs.tf
output "LB-DNS-NAME" {
  value = aws_lb.alb.dns_name
}