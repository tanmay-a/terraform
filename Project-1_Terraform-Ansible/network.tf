# Create VPC in us-east-1
resource "aws_vpc" "vpc_master" {
  provider             = aws.master
  cidr_block           = "10.0.0.0/16"
  enable_dns_support   = true
  enable_dns_hostnames = true
  tags = {
    Name = "Master-vpc-jenkins"
  }
}

# Create VPC in us-west-2
resource "aws_vpc" "vpc_master_oregon" {
  provider             = aws.worker
  cidr_block           = "192.168.0.0/16"
  enable_dns_support   = true
  enable_dns_hostnames = true
  tags = {
    Name = "Worker-vpc-jenkins"
  }
}

# Create Internet Gateway in us-east-1
resource "aws_internet_gateway" "igw_master" {
  provider = aws.master
  vpc_id   = aws_vpc.vpc_master.id
  tags = {
    Name = "igw_master"
  }
}

# Create Internet Gateway in us-west-2
resource "aws_internet_gateway" "igw_master_oregon" {
  provider = aws.worker
  vpc_id   = aws_vpc.vpc_master_oregon.id
  tags = {
    Name = "igw_worker"
  }
}


#Get all available AZ in VPC master region

data "aws_availability_zones" "azs" {
  provider = aws.master
  state    = "available"
}

# Create subnet-1 in VPC master

resource "aws_subnet" "subnet_1" {
  provider          = aws.master
  availability_zone = data.aws_availability_zones.azs.names[0]
  vpc_id            = aws_vpc.vpc_master.id
  cidr_block        = "10.0.1.0/24"
  tags = {
    Name = "subnet_1"
  }
}

# Create subnet-2 in VPC master
resource "aws_subnet" "subnet_2" {
  provider          = aws.master
  availability_zone = data.aws_availability_zones.azs.names[1]
  vpc_id            = aws_vpc.vpc_master.id
  cidr_block        = "10.0.2.0/24"
  tags = {
    Name = "subnet_2"
  }
}

# Create subnet-1 in VPC Worker

resource "aws_subnet" "subnet_1_oregon" {
  provider   = aws.worker
  vpc_id     = aws_vpc.vpc_master_oregon.id
  cidr_block = "192.168.1.0/24"
  tags = {
    Name = "subnet_1_oregon"
  }
}

#Initiating VPC Peering from us-east-1

resource "aws_vpc_peering_connection" "peer" {
  provider    = aws.master
  vpc_id      = aws_vpc.vpc_master.id
  peer_vpc_id = aws_vpc.vpc_master_oregon.id
  peer_region = var.worker
  auto_accept = false

  tags = {
    Side = "Requester"
  }
}

# Accepter's side of the connection.
resource "aws_vpc_peering_connection_accepter" "peer" {
  provider                  = aws.worker
  vpc_peering_connection_id = aws_vpc_peering_connection.peer.id
  auto_accept               = true

  tags = {
    Side = "Accepter"
  }
}

#Create Route table for us-east-1

resource "aws_route_table" "master_route" {
  provider = aws.master
  vpc_id   = aws_vpc.vpc_master.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.igw_master.id
  }
  route {
    cidr_block                = "192.168.0.0/16"
    vpc_peering_connection_id = aws_vpc_peering_connection.peer.id
  }
  lifecycle {
    ignore_changes = all
  }
  tags = {
    "name" = "master-region-rt"
  }
}

#Overwrite the default route table created at the time of Master VPC creation.
resource "aws_main_route_table_association" "set_master_route" {
  provider       = aws.master
  vpc_id         = aws_vpc.vpc_master.id
  route_table_id = aws_route_table.master_route.id
}

##Create Route table for us-west-2

resource "aws_route_table" "worker_route" {
  provider = aws.worker
  vpc_id   = aws_vpc.vpc_master_oregon.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.igw_master_oregon.id
  }
  route {
    cidr_block                = "10.0.0.0/16"
    vpc_peering_connection_id = aws_vpc_peering_connection.peer.id
  }
  lifecycle {
    ignore_changes = all
  }
  tags = {
    "name" = "worker-region-rt"
  }
}

#Overwrite the default route table created at the time of Worker VPC creation.
resource "aws_main_route_table_association" "set_worker_route" {
  provider       = aws.worker
  vpc_id         = aws_vpc.vpc_master_oregon.id
  route_table_id = aws_route_table.worker_route.id
}