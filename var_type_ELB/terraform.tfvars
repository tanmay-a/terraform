/*The file name should be terraform.tfvars and incase we need to change it custom then we need to define
it in cli while running terraform apply -var-file="custom.tfvars"
Also the following value can be assigned from os env variables as TF_VARS_intance_type="m5.large"
*/
instance_type="t2.medium"
az=["ap-south-1a","ap-south-1b","ap-south-1c"]
