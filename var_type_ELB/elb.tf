# Create a new load balancer
provider "aws" {
  region                  = var.region
  shared_credentials_file = "/Users/apple/.aws/credentials"
  profile                 = "default"
}

#Example of Nested variable in AMI filed fo EC2

resource "aws_instance" "myec2" {
  ami = var.ami["${var.region}"]
  instance_type = var.instance_type
}

resource "aws_elb" "test" {
  name               = "terraform-elb"
  availability_zones = var.az

  listener {
    instance_port     = 8000
    instance_protocol = "http"
    lb_port           = 80
    lb_protocol       = "http"
  }

  health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 3
    target              = "HTTP:8000/"
    interval            = 30
  }

  cross_zone_load_balancing   = true
  idle_timeout                = var.timeout
  connection_draining         = true
  connection_draining_timeout = var.timeout

  tags = {
    Name = "terraform-elb"
  }
}
