terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = ">=3.28.0"
    }
    github = {
      source = "integrations/github"
      version = "~> 4.0"
    }
  }
}
provider "aws" {
  region                  = "ap-south-1"
  shared_credentials_file = "/Users/apple/.aws/credentials"
  profile                 = "default"
}
