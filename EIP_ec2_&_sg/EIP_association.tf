
resource "aws_eip" "one" {
  vpc      = true
}

resource "aws_instance" "myec2" {
  ami = "ami-08e0ca9924195beba"
  instance_type = var.instance_type
}

resource "aws_eip_association" "eip_assoc" {
  instance_id   = aws_instance.myec2.id
  allocation_id = aws_eip.one.id
}

resource "aws_security_group" "linux_ssh" {
  name        = "linux_ssh"
  description = "Allow SSH inbound traffic from aws_eip"

  ingress {
    description = "SSH From EIP"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["${aws_eip.one.public_ip}/32"]
  }
  ingress {
    description = "SSH From my ip"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = [var.my_ip]
  }
#Below Block will allow all the public outbound connections
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "allow_ssh"
  }
}
