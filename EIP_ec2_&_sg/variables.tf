/*
1. Incase no default value is assigned, it will ask for the value during run time.
2. Second point to note that the variable values first will be pick from the tfvars
   and incase it is not defined then it will take the default values
3. Variable type is important incase we want to restrict the data type
   [sting,list,map,number]
*/
variable "my_ip" {
  default = "223.236.107.62/32"
}
variable "instance_type" {
  type = string
  default = "t2.micro"
}
