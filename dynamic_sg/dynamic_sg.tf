terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = ">=3.28.0"
    }
  }
}
provider "aws" {
  region                  = "ap-south-1"
  shared_credentials_file = "/Users/apple/.aws/credentials"
  profile                 = "default"
}

resource "aws_iam_user" "dev" {
  name = "iamuser.${count.index}"
  count = 3
  path = "/system/"
}

# Using splat function to output iam details

output "iam_arns" {
  value = aws_iam_user.dev[*].arn
}
output "iam_names" {
  value = aws_iam_user.dev[*].name
}


variable "sg_ports" {
  type = list(number)
  description = "List of ingress ports"
  default = [3434, 3389, 4433, 5465, 7676, 9900]
}

resource "aws_security_group" "dynamicsg" {
  name = "dynamic_sg"
  description = "ingress for vault"

  dynamic "ingress" {
    for_each = var.sg_ports
    iterator = port
    content {
      from_port   = port.value
      to_port     = port.value
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    }
  }
  dynamic "egress" {
    for_each = var.sg_ports
    iterator = port
    content {
      from_port   = port.value
      to_port     = port.value
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    }
  }
}
