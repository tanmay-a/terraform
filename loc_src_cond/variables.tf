/*
1. Incase no default value is assigned, it will ask for the value during run time.
2. Second point to note that the variable values first will be pick from the tfvars
   and incase it is not defined then it will take the default values
3. Variable type is important incase we want to restrict the data type
   [sting,list,map,number]
*/
variable "my_ip" {
  default = "122.169.18.199/32"
}
variable "region" {
#  type    = list
  default = "us-east-1"
}
variable "instance_type" {
  type = string
  default = "t3.micro"
}
variable "ami" {
  type = map
  default = {
    ap-south-1 = "ami-08e0ca9924195beba"
    us-east-1  = "ami-047a51fa27710816e"
    us-west-2  = "ami-0e999cbd62129e3b1"
  }
}
variable "istest"{
  type = bool
}
variable "tags" {
  type = list
  default = ["First_ec2" , "Second_ec2"]
}
