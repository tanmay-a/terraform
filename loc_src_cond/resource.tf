terraform {
  required_version = " > 0.12"
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = ">=3.28.0"
    }
  }
}
provider "aws" {
  region                  = "ap-south-1"
  shared_credentials_file = "/Users/apple/.aws/credentials"
  profile                 = "default"
}

locals {
  common_tags = {
    Owner = "DevOps Team"
    service = "backend"
  }
  time = formatdate ("DD MMM YYYY hh:mm ZZZ", timestamp())
}

data "aws_ami" "amzn_linux" {
  most_recent      = true
  # owners           = ["amazon"]
  #owners           = ["309956199498"]
  owners           = ["099720109477"]

  filter {
    name   = "name"
    # values = ["amzn2-ami-hvm*"]
    #values = ["RHEL-8*"]
    values = ["*ubuntu-bionic-18.04-amd64-server-*"]
  }
  filter {
    name   = "root-device-type"
    values = ["ebs"]
  }
  filter {
    name   = "architecture"
    values = ["x86_64"]
  }
  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
}

resource "aws_key_pair" "tanmay" {
  key_name   = "tanmay"
  public_key = "file(/Users/apple/.ssh/id_rsa.pub)"
}

resource "aws_key_pair" "tanmay_prod" {
  key_name   = "tanmay_prod"
  public_key = file("${path.module}/id_rsa.pub")
}

resource "aws_instance" "function_use" {
  ami = lookup(var.ami, var.region)
  instance_type = var.instance_type
  key_name = aws_key_pair.tanmay.key_name
  count = var.istest?2:0
  tags = {
    Name = element(var.tags, count.index)
  }
}
resource "aws_instance" "myec2" {
  ami = data.aws_ami.amzn_linux.id
  instance_type = "t2.micro"
  key_name = aws_key_pair.tanmay_prod.key_name
  count = var.istest?0:1
  provisioner "remote-exec" {
    inline = [
      "sudo apt-get update",
      "sudo apt-get install -y nginx",
      "sudo systemctl start nginx",
      "sudo systemctl enable nginx"
    ]
    connection {
      type = "ssh"
      host = self.public_ip
      user = "ubuntu"
      private_key = file("/Users/apple/.ssh/id_rsa")
    }
  }
  tags = local.common_tags
}
output "timestamp" {
  value = local.time
}
